package com.example.unittesting.login.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class LoginValidatorTest {

    val objectUnderTest = LoginValidator()

    @Test
    fun `o login vazio é inválido`() {
        //when
        val result = objectUnderTest.validateLogin("")
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `o login não vazio é válido`() {
        //when
        val result = objectUnderTest.validateLogin("anyLogin")
        //then
        assertThat(result).isTrue()
    }

    @Test
    fun `a senha vazia é inválida`() {
        //when
        val result = objectUnderTest.validatePassword("")
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `a senha é inválida se for menor que o limite`() {
        //when
        val result = objectUnderTest.validatePassword("12345")
        //then
        assertThat(result).isFalse()
    }

    @Test
    fun `senha é válida se igual a limite`() {
        //when
        val result = objectUnderTest.validatePassword("123456")
        //then
        assertThat(result).isTrue()
    }

    @Test
    fun `a senha é válida se for maior que o limite`() {
        //when
        val result = objectUnderTest.validatePassword("1234567")
        //then
        assertThat(result).isTrue()
    }
}