package com.example.unittesting.login.model

import com.example.unittesting.login.model.LoginRepository.Companion.CORRECT_LOGIN
import com.example.unittesting.login.model.LoginRepository.Companion.CORRECT_PASSWORD
import org.junit.Test

class LoginRepositoryTest {

    val objectUnderTest = LoginRepository()

    @Test
    fun `faça o login com login e senha corretos`() {
        //given
        val login = CORRECT_LOGIN
        val password = CORRECT_PASSWORD
        //when
        val result = objectUnderTest.login(login, password)
        //then
        result.test().await()
                .assertResult(true)
    }

    @Test
    fun `não faça o login com apenas o login correto`() {
        //given
        val login = CORRECT_LOGIN
        val password = "anyPassword"
        //when
        val result = objectUnderTest.login(login, password)
        //then
        result.test().await()
                .assertResult(false)
    }

    @Test
    fun `não faça o login com apenas a senha correta`() {
        //given
        val login = "anyLogin"
        val password = CORRECT_PASSWORD
        //when
        val result = objectUnderTest.login(login, password)
        //then
        result.test().await()
                .assertResult(false)
    }
}