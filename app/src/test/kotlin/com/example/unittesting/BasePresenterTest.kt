package com.example.unittesting

import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.concurrent.TimeUnit

class BasePresenterTest {

    val objectUnderTest = BasePresenter<TestView>()

    @Test
    fun `Está limpando os observables quando fechar a tela`() {
        //given
        val disposable = Observable.interval(100, TimeUnit.MILLISECONDS).subscribe()
        objectUnderTest.bindToLifecycle(disposable)
        //when
        objectUnderTest.destroyView()
        //then
        assertThat(disposable.isDisposed).isTrue()
    }

    @Test
    fun `Referência de visualização limpa na visualização de destruição`() {
        //given
        objectUnderTest.createView(TestView())
        //when
        objectUnderTest.destroyView()
        //then
        assertThat(objectUnderTest.view).isNull()
    }
}

class TestView {

}